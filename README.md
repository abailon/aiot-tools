# AIoT tools

The AIoT tools are a set of tools to flash, control or configure MediaTek
boards, and in particular the Pumpkin Evaluation Boards.

Please check the full documentation at
http://mediatek.gitlab.io/aiot/bsp/aiot-tools/
